/* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
particlesJS.load("particles-js", "/lib/particlesjs-config.json", function () {
  console.log("callback - particles.js config loaded");
});

document.querySelector("#particles-js").style.height = '85vh';